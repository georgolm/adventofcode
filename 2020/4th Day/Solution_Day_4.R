library(tidyverse)

input <- read_csv("4th Day/Input.csv", col_names = FALSE)

# ---- Task 1 check Passport validity---
validity_t1 = c("byr:", "iyr:", "eyr:", "hgt:", "hcl:", "ecl:", "pid:")


validity_check <- function(passport_string, validator){
  n = length(validator)
  validator_count <- 0
  passport_string = as.character(passport_string)
  
  for (i in 1:n) {

    validator_string <- validator[i]
    if (grepl(validator_string, passport_string) == TRUE) {
      validator_count = validator_count + 1
    } else { 
      next
      }
    }
  
  if(validator_count == 7){
    return(1)
  }else {
    return(0)
  }
}

input <- input %>% 
  mutate(validation = unlist(map(X1, ~validity_check(., validator = validity_t1))))

table(input$validation)


#---- Task 1 from David Robinson Solution

# input = tibble(x = readLines("4th Day/input_2.txt"))
# required = c("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid")

# fields <- input %>%
#   mutate(passport = cumsum(x== "")) %>%
#   mutate(m = str_match_all(x, "(...)\\:([^ ]+)")) %>%
#   mutate(f = map(m, ~.[, 2]),
#          v = map(m, ~.[, 3])) %>%
#   unnest(c(f,v)) %>%
#   filter(f %in% required) %>% 
#   select(-x,-m)

# fields %>%
#   count(passport) %>%
#   summarize(answer = sum(n == 7))


# ---- Task 2 Value Validity David Robinson ----

# t <- fields %>% 
#   extract(v, c("value", "unit"), "(\\d+)(cm|in)", remove = FALSE, convert = TRUE) %>% 
#   mutate(valid = case_when(f == "byr" ~ between(as.integer(v), 1920, 2002),
#                            f == "iyr" ~ between(as.integer(v), 2010, 2020),
#                            f == "eyr" ~ between(as.integer(v), 2030, 2030),
#                            f == "hgt" ~ ifelse(unit == "cm", between(value, 150,193), 
#                                                between(value, 59,76)), 
#                            f == "hcl" ~ str_detect(v, "^#[0-9a-f]{6}$"),
#                            f == "ecl" ~ v %in% c("amb", "blu", "brn", "gry", "grn", "hzl", "oth"),
#                            f == "pid" ~ str_detect(v, "^[0-9]{9}$"))) %>% 
#   filter(valid) %>% 
#   count(passport) %>% 
#   summarise(answer = sum(n == 7))

