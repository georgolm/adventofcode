library(tidyverse)

input = tibble(x = readLines("5th Day/input.txt")) %>% 
  extract(x, as.character(1:10), "(.)(.)(.)(.)(.)(.)(.)(.)(.)(.)", remove = TRUE, convert = TRUE) %>% 
  mutate(row = 0,
         column = 0)

# ---- Task 1 ----

for (i in 1:nrow(input)) {
  
  rows = as.vector(seq(0, 127, 1))
  
  for (j in 1:7) {
    n_rows = length(rows) / 2
    factor_vector = c(rep(1, n_rows), rep(2, n_rows))
    split_rows <- split(rows, factor_vector)
    
    if (input[i,j] == "F") {
      rows = split_rows$`1`
    } else {rows = split_rows$`2`}
  }
  
  input[i, 11] <- rows
}

for (i in 1:nrow(input)) {
  
  cols = as.vector(seq(0, 7, 1))
  
  for (j in 8:10) {
    n_cols = length(cols) / 2
    factor_vector = c(rep(1, n_cols), rep(2, n_cols))
    split_cols <- split(cols, factor_vector)
    
    if (input[i,j] == "L") {
      cols = split_cols$`1`
    } else {cols = split_cols$`2`}
  }
  
  input[i, 12] <- cols
}

input %>% 
  mutate(sanity = row * 8 + column) %>% 
  arrange(desc(sanity)) %>% 
  select(sanity) %>% 
  head(1)


#---- Task 2 ----

my_row <- input %>% 
  group_by(row) %>% 
  summarise(n = n()) %>% 
  filter(n == 7) %>% 
  select(row) %>% 
  as.numeric()

  