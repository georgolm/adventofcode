---
title: "AOC24"
format:
  html:
    self-contained: true
editor_options: 
  chunk_output_type: inline
---

## Day One

**Task 1**
```{r}
list_difference <- readr::read_delim("day_one_input.txt",
                                     col_names = FALSE, show_col_types = FALSE) |> 
  dplyr::transmute(ll_sorted = sort(X1),
                   rl_sorted = sort(X4),
                   list_diff = abs(ll_sorted - rl_sorted)) |> 
  dplyr::summarise(sum(list_diff))

print(paste("The difference between the two lists is", list_difference))
```
**Task 2**
```{r}
lists <- readr::read_delim("day_one_input.txt",
                                     col_names = c("left", "space", "tab", "right"), show_col_types = FALSE,
                                     col_select = c(left,right))

right_list_grouped <- lists |> 
  dplyr::group_by(right) |> 
  dplyr::summarise(n = dplyr::n())

adjusted_list_difference <- lists |> 
  dplyr::left_join(right_list_grouped, by = c("left" = "right")) |> 
  dplyr::mutate(adj_values = left * tidyr::replace_na(n,0)) |>
  dplyr::summarise(sum(adj_values))


print(paste("The difference between the two lists is", adjusted_list_difference))
```
